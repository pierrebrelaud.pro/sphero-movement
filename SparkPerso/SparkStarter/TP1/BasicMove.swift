//
//  BasicMove.swift
//  SparkPerso
//
//  Created by Guillaume Lagouy on 18/10/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import Foundation

class BasicMove{
    var durationInSec:Float
    var speed:Float
    var direction:Direction
    var heading:Double = 0.0
    
    enum Direction{
        case front, back, rotateLeft, rotateRight, up, down, translateLeft, translateRight, stop
    }
    var description:String{
        get{
            return "Move \(direction) during \(durationInSec)s with \(speed) speed"
        }
    }
    
    init(durationInSec:Float, direction:Direction, speed:Float) {
        self.durationInSec  = durationInSec
        self.direction      = direction
        self.speed          = speed
    }
}





class Front:BasicMove{
    init(durationInSec:Float) {
        super.init(durationInSec: durationInSec, direction: .front, speed: 0.5)
    }
}

class RotateRight90:BasicMove{
    let duration:Float = 4.5
    init() {
        super.init(durationInSec: duration, direction: .rotateRight, speed: 0.5)
    }
}

class RotateLeft90:BasicMove{
    let duration:Float = 4.5
    init() {
        super.init(durationInSec: duration, direction: .rotateLeft, speed: 0.5)
    }
}



class RotateRight1:BasicMove{
    init(duration:Float, speed:Float) {
        super.init(durationInSec: duration, direction: .rotateRight, speed: speed)
    }
}

class RotateLeft:BasicMove{
    init(duration:Float, speed:Float) {
        super.init(durationInSec: duration, direction: .rotateLeft, speed: speed)
    }
}

class Stop1:BasicMove{
    init() {
        super.init(durationInSec: 0.0, direction: .stop, speed: 0)
    }
}

class SpheroMove:BasicMove{
    init(heading:Double, durationInSec: Float, speed: Float) {
        super.init(durationInSec: durationInSec, direction: .front, speed: speed)
        self.heading = heading
    }
}
