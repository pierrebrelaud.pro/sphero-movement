//
//  TP1ViewController.swift
//  SparkPerso
//
//  Created by Guillaume Lagouy on 17/10/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import UIKit
import DJISDK

class TP1ViewController: UIViewController {
    var sparkMovementManager:SparkMovementManager? = nil
    
    @IBOutlet weak var sequenceTextView: UITextView!
    
    var sequence = [BasicMove](){
        didSet{
            DispatchQueue.main.async{
                self.displaySequenceOnTextView()
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*let seq = [Front(durationInSec: 0.5),
                   RotateRight90(),
                   Front(durationInSec: 0.5)]
        
        sparkMovementManager =  SparkMovementManager(sequence: seq)*/

    }
    
    @IBAction func stopButtonClicked(_ sender: Any) {
        print("Stop")
        stop()
        sparkMovementManager?.redAlert()
    }
    
    @IBAction func startButtonClicked(_ sender: Any) {
        print("Start")

        sparkMovementManager?.playSequence()
    }
    
    func stop() {
        if let mySpark = DJISDKManager.product() as? DJIAircraft {
            mySpark.mobileRemoteController?.leftStickVertical = 0.0
            mySpark.mobileRemoteController?.leftStickHorizontal = 0.0
            mySpark.mobileRemoteController?.rightStickHorizontal = 0.0
            mySpark.mobileRemoteController?.rightStickVertical = 0.0
        }
    }
    
    @IBAction func rotateLeftClicked(_ sender: Any) {
        self.askForDurationAndSpeed{(speed, duration) in
            self.sequence.append(RotateLeft(duration: duration, speed: speed))
        }
    }
    
    @IBAction func frontClicked(_ sender: Any) {
        self.askForDurationAndSpeed{(speed, duration) in
            self.sequence.append(Front(durationInSec: duration))
        }
    }
    
    @IBAction func rotateRightClicked(_ sender: Any) {
        self.askForDurationAndSpeed{(speed, duration) in
            self.sequence.append(RotateRight1(duration: duration, speed: speed))
        }
    }
    
    @IBAction func displaySequenceClicked(_ sender: Any) {
        displaySequenceOnTextView()
        
    }
    
    @IBAction func clearSequenceClicked(_ sender: Any) {
        sequence = []
        sequenceTextView.text = ""
        sparkMovementManager?.clearSequence()
    }
    
    func displaySequenceOnTextView(){
        sparkMovementManager = SparkMovementManager(sequence: sequence)
        if let desc = sparkMovementManager?.sequenceDescription(){
            sequenceTextView.text = desc
        }
    }
}
