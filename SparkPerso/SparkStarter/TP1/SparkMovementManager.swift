//
//  SparkMovementManager.swift
//  SparkPerso
//
//  Created by Guillaume Lagouy on 18/10/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import Foundation
import DJISDK

class SparkMovementManager:GenericMovementManager {
    override init(sequence: [BasicMove]) {
        super.init(sequence: sequence)
        self.sequence.append(Stop1())
    }
    
    override func playMove(move: BasicMove, moveDidFinish: @escaping (() -> ())) {
        talkWithSDK(move: move)
        print(move.description)
        delay(move.durationInSec){
            moveDidFinish()
        }
    }
    
    func talkWithSDK(move:BasicMove) {
        if let mySpark = DJISDKManager.product() as? DJIAircraft {
            mySpark.mobileRemoteController?.leftStickVertical = 0.0
            mySpark.mobileRemoteController?.leftStickHorizontal = 0.0
            mySpark.mobileRemoteController?.rightStickHorizontal = 0.0
            mySpark.mobileRemoteController?.rightStickVertical = 0.0
            
            let speed = move.speed
            
            switch move {
            case is Front:
                mySpark.mobileRemoteController?.rightStickVertical = speed
                mySpark.mobileRemoteController?.leftStickVertical = -speed
                
            case is RotateLeft:
                mySpark.mobileRemoteController?.leftStickHorizontal = -speed
            case is RotateRight90:
                mySpark.mobileRemoteController?.leftStickHorizontal = speed
            case is RotateLeft90:
                mySpark.mobileRemoteController?.leftStickHorizontal = -speed
            case is Stop:
                mySpark.mobileRemoteController?.leftStickVertical = 0.0
                mySpark.mobileRemoteController?.leftStickHorizontal = 0.0
                mySpark.mobileRemoteController?.rightStickHorizontal = 0.0
                mySpark.mobileRemoteController?.rightStickVertical = 0.0
            default:
                break
            }
        }
    }
}
