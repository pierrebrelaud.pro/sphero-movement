//
//  SensorControlViewController.swift
//  SparkPerso
//
//  Created by AL on 01/09/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import UIKit
import simd

class SpheroSensorControlViewController: UIViewController {
    
    @IBOutlet weak var gyroChart: GraphView!
    @IBOutlet weak var acceleroChart: GraphView!
    var mouvementData = [Classes:[[Double]]]()
    var selectedClass = Classes.Carre
    var isRecording = false
    var isPredicting = false
    
    enum Classes:Int {
        case Carre, Triangle, Rond
        
        func neuralNetResponse() -> [Double] {
            switch self {
                case .Carre: return [1.0, 0.0, 0.0]
                case .Triangle: return [0.0, 1.0, 0.0]
                case .Rond: return [0.0, 0.0, 1.0]
            }
        }
        
    }
    
    var neuralNet:NeuralNet? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let structure = try! NeuralNet.Structure(nodes: [3600, 30, 3], hiddenActivation: .rectifiedLinear, outputActivation: .rectifiedLinear)
        
        neuralNet = try! NeuralNet(structure: structure)
        
        
        mouvementData[.Carre] = []
        mouvementData[.Triangle] = []
        mouvementData[.Rond] = []
        
        var currentData = [Double]()
        
        SharedToyBox.instance.bolt?.sensorControl.enable(sensors: SensorMask.init(arrayLiteral: .accelerometer,.gyro))
        SharedToyBox.instance.bolt?.sensorControl.interval = 1
        SharedToyBox.instance.bolt?.setStabilization(state: SetStabilization.State.off)
        
        SharedToyBox.instance.bolt?.sensorControl.onDataReady = { data in
            DispatchQueue.main.async {
                if self.isRecording || self.isPredicting {
                    
                    if let acceleration = data.accelerometer?.filteredAcceleration {
                        // PAS BIEN!!!
                        
                        currentData.append(contentsOf: [acceleration.x!, acceleration.y!, acceleration.z!])
                        
                        let dataToDisplay: double3 = [acceleration.x!, acceleration.y!, acceleration.z!]
                        
                        self.acceleroChart.add(dataToDisplay)
                    }
                    
                    let divide:Double = 1000.0
                    
                    if let gyro = data.gyro?.rotationRate {
                        // TOUJOURS PAS BIEN!!!
                        currentData.append(contentsOf: [Double(gyro.x!)/divide, Double(gyro.y!)/divide, Double(gyro.z!)/divide])
                        let rotationRate: double3 = [Double(gyro.x!)/divide, Double(gyro.y!)/divide, Double(gyro.z!)/divide]
                        self.gyroChart.add(rotationRate)
                    }
                    
                    //count à changer pour le nombre de données voulues
                    if currentData.count >= 180 {
                        if self.isRecording {
                            self.isRecording = false
                            print("data ready for network!")
                            self.mouvementData[self.selectedClass]?.append(currentData)
                            currentData = []
                        }
                        
                        if self.isPredicting {
                            self.isPredicting = false
                            let floatInput = currentData.map{ Float($0) }
                            let prediction = try! self.neuralNet?.infer(floatInput)
                            print(prediction)
                            currentData = []
                        }
                        //po self.mouvementData.Carre?count
                        //po self.mouvementData.Carre?[0]
                        
                    }
                }
            }
        }
        
    }
    
    func trainNetwork() {
        
        //----------------------------
        //training
        //----------------------------
        for _ in 0...80 {
            if let selectedClass = mouvementData.randomElement(),
                let data = selectedClass.value.randomElement() {
                let expectedResponse = selectedClass.key.neuralNetResponse()
                
                let floatInput = data.map{ Float($0) }
                let floatRes = expectedResponse.map{ Float($0) }
                
                try! neuralNet?.infer(floatInput)
                try! neuralNet?.backpropagate(floatInput)
            }
        }
        
        //----------------------------
        //validation
        //----------------------------
        for k in mouvementData.keys {
            let values = mouvementData[k]!
            for v in values {
                let floatInput = v.map{ Float($0) }
                let prediction = try! neuralNet?.infer(floatInput)
                print(prediction!)
            }
        }
    }
    
    @IBAction func startButtonClicked(_ sender: Any) {
        self.isRecording = true
    }
    
    
    @IBAction func segmentedControlChanged(_ sender: UISegmentedControl) {
        let index = sender.selectedSegmentIndex
        if let s = Classes(rawValue: index) {
            selectedClass = s
        }
        
    }
    
    @IBAction func predictButtonClicked(_ sender: Any) {
        self.isPredicting = true
    }
    
    @IBAction func trainButtonClicked(_ sender: Any) {
        trainNetwork()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        SharedToyBox.instance.bolt?.sensorControl.disable()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
