//
//  SpheroLedsViewController.swift
//  SparkPerso
//
//  Created by AL on 01/09/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import UIKit

class LedViewController: UIViewController {
    
    
    
    @IBAction func click(_ sender: Any) {
        Led().initLedSphero()
        Led().rotateSphero(type: "correct", shape: Led().square, color: .white)
        //Led.rotateSphero()
    }
    
}
