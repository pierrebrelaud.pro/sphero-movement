//
//  DrawViewController.swift
//  SparkPerso
//
//  Created by Pierre Brelaud on 18/11/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import UIKit
import SocketIO

class DrawViewController: UIViewController {
    
    @IBOutlet weak var sequencesStackView: UIStackView!
    @IBOutlet weak var spheroChoice: UISegmentedControl!
    //current sphero
    //var sphero: String = "SB-42C1"
    
    //socket
    let manager = SocketManager(socketURL: URL(string: "http://172.28.59.28:8083")!, config: [.log(true), .compress])
    
    //heading label
    @IBOutlet weak var headingLabel: UILabel!
    
    //manager
    var drawManager:DrawManager? = nil
    
    //timer data
    var punchInTime : UserDefaults = UserDefaults.standard
    var start: CFAbsoluteTime!
    
    //heading
    var heading:Double = 0.0 {
        didSet{ displayHeading() }
    }
    
    //rotate duration
    let rotateDuration:Float = 0.26
    let rotateDuration0:Float = 0.25
    let rotateDuration1:Float = 0.50
    let rotateDuration2:Float = 0.75
    
    let rotate45Duration:Float = 0.4
    //increase value rotate
    var increase:Double = 90/4.68
    
    //sequence
    var sequence = [Draw]()
    var sequences = [[Draw]]()
    
    struct SpheroStruct {
        let name:String
        let sphero:BoltToy?
        var heading:Double
        var tempHeading:Double
        var initHeading:Double
    }
    
    var sphero1:SpheroStruct?
    var sphero2:SpheroStruct?
    var sphero3:SpheroStruct?
    var currentSphero:SpheroStruct?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let socket = manager.defaultSocket

        socket.emit("test", "plop")
        
        socket.on("test", callback: {_,_ in
            print("received")
        })
        /*socket.on("test") {data, ack in
            print("-----------")
            print(data)
            print("-----------")
        }*/

        socket.connect()
        
        
        //set heaging color
        SharedToyBox.instance.bolts.map {
            $0.setBackLed(color: .darkGray)
        }
        //set default sphero
        setCurrentSphero(index:spheroChoice.selectedSegmentIndex);
        
        //set sphero list
        SharedToyBox.instance.bolts.map {
            if($0.peripheral?.name == "SB-A729") {
                sphero1 = SpheroStruct(name: "SB-A729", sphero: $0, heading: 0.0, tempHeading: 0.0, initHeading: 0.0)
            }
            if($0.peripheral?.name == "SB-313C") {
                sphero2 = SpheroStruct(name: "SB-313C", sphero: $0, heading: 0.0, tempHeading: 0.0, initHeading: 0.0)
            }
            if($0.peripheral?.name == "SB-42C1") {
                sphero3 = SpheroStruct(name: "SB-42C1", sphero: $0, heading: 0.0, tempHeading: 0.0, initHeading: 0.0)
            }
        }
    }

    @IBAction func startButtonClicked(_ sender: Any) {
        start = CFAbsoluteTimeGetCurrent()
        //------------ action
        currentSphero?.sphero?.roll(heading: currentSphero!.heading, speed: 250)
    }
    
    @IBAction func stopButtonClicked(_ sender: Any) {
        let elapsed = CFAbsoluteTimeGetCurrent() - start
        //------------ register sequence
        sequence.append(Forward(duration: Float(elapsed), heading: currentSphero!.heading, sphero: currentSphero!.sphero!))
        sequence.append(Stop(heading: currentSphero!.heading, sphero: currentSphero!.sphero!))
        //------------ action
        currentSphero?.sphero?.stopRoll(heading: currentSphero!.heading)
    }
    
    @IBAction func playSequenceClicked(_ sender: Any) {
        drawManager = DrawManager(sequence: sequence)
        drawManager?.playSequence()
    }
    
    @IBAction func clearSequenceClicked(_ sender: Any) {
        sequence = []
        drawManager?.clearSequence()
    }
    
    @IBAction func stopAllClicked(_ sender: Any) {
        currentSphero?.sphero?.stopRoll(heading: currentSphero!.heading)
    }
    
    @IBAction func rotateRightClicked(_ sender: UIButton) {
        print("rotate right 90")
        
        let rotDur = setRotateDuration(s: sender);
        //------------ register sequence
        currentSphero!.heading += 90
        currentSphero!.tempHeading = currentSphero!.heading
        //do 90
        sequence.append(Heading(duration: 1.2, heading
            : currentSphero!.heading, sphero: currentSphero!.sphero!))
        currentSphero!.heading += increase + 16
        //move
        sequence.append(Rotate90Right(duration: rotDur, heading: currentSphero!.heading, sphero: currentSphero!.sphero!))
        //stop
        sequence.append(Stop(heading: currentSphero!.heading, sphero: currentSphero!.sphero!))
        
        //------------ action
        currentSphero!.sphero?.rotateAim(currentSphero!.heading)
        
        delay(1.2) {
            self.currentSphero!.sphero?.roll(heading: self.currentSphero!.heading, speed: 250)
            delay(rotDur) {
                self.currentSphero!.sphero?.stopRoll(heading: self.currentSphero!.heading)
            }
        }
            
    }
    
    @IBAction func rotateRight45Clicked(_ sender: Any) {

        //------------ register sequence
        currentSphero!.tempHeading = currentSphero!.heading + 45
        currentSphero!.heading += 90
        //do 90
        sequence.append(Heading(duration: 1.2, heading: currentSphero!.heading, sphero: currentSphero!.sphero!))
        heading += increase + 16
        //move
        sequence.append(Rotate45Right(duration: rotate45Duration, heading: currentSphero!.heading, sphero: currentSphero!.sphero!))
        //stop
        sequence.append(Stop(heading: currentSphero!.heading, sphero: currentSphero!.sphero!))
        
        //------------ action
        currentSphero!.sphero?.rotateAim(currentSphero!.heading)
        
        delay(1.2) {
            self.currentSphero!.sphero?.roll(heading: self.currentSphero!.heading, speed: 250)
            delay(self.rotate45Duration) {
                self.currentSphero!.sphero?.stopRoll(heading: self.currentSphero!.heading)
            }
        }
    }
    
    @IBAction func resetTempHeadingClicked(_ sender: Any) {
        
        currentSphero!.heading = currentSphero!.tempHeading
        sequence.append(Heading(duration: 2, heading: currentSphero!.heading, sphero: currentSphero!.sphero!))
        //------------ action
        currentSphero!.sphero?.stopRoll(heading: self.currentSphero!.heading)
    }
    
    func setRotateDuration(s:UIButton) -> Float {
        
        if(s.tag == 0) {
            return rotateDuration0
        }
        else if (s.tag == 1) {
            return rotateDuration1
        }
        else {
            return rotateDuration2
        }
    }
    
    
    @IBAction func headingSlider(_ sender: UISlider) {
        currentSphero?.heading = Double(sender.value)
        currentSphero!.sphero?.rotateAim(currentSphero!.heading)
        heading = Double(sender.value)
    }
    
    func displayHeading() {
        headingLabel.text = "\(heading)"
    }
    
    @IBAction func spheroPickerChanged(_ sender: UISegmentedControl) {
        setCurrentSphero(index:sender.selectedSegmentIndex);
    }
    
    func saveCurrentSphero(name:String) {
        
        if(name == "SB-A729") {
            sphero1 = currentSphero
        }
        if(name == "SB-313C") {
            sphero2 = currentSphero
        }
        if(name == "SB-42C1") {
            sphero3 = currentSphero
        }
    }
    
    func setCurrentSphero(index:Int) {
        
        if(currentSphero != nil) {
            saveCurrentSphero(name: currentSphero!.name)
        }
        
        var id = "";
        
        if (index == 0) {
            id = "A729"
            currentSphero = sphero1
        }
        if (index == 1) {
            id = "313C"
            currentSphero = sphero2
        }
        if (index == 2) {
            id = "42C1"
            currentSphero = sphero3
        }
    }
    
    @IBAction func saveInitHeading(_ sender: Any) {
        let headingToSave = currentSphero!.heading
        currentSphero?.initHeading = headingToSave
        sphero2?.initHeading = headingToSave
    }
    
    @IBAction func resetInitHeading(_ sender: Any) {
        let headingToReset = currentSphero!.initHeading
        currentSphero?.heading = headingToReset
        currentSphero?.sphero?.rotateAim(headingToReset)
        
        print("saved : \(currentSphero?.initHeading) -> new : \(currentSphero?.heading)")
    }
    
    @IBAction func saveSequence(_ sender: Any) {
        
        //create button
        let button = UIButton()
        button.setTitle("sequence \(sequences.count)", for: .normal)
        button.backgroundColor = UIColor.darkGray
        button.addTarget(self, action: #selector(playSequence(_:)), for: .touchUpInside)
        button.tag = sequences.count
        
        //save sequence
        sequencesStackView.addArrangedSubview(button)
        sequences.append(sequence)
        
        //reset sequence
        sequence = []
        drawManager?.clearSequence()
    }
    
    
    @objc func playSequence(_ sender: UIButton) {
        drawManager = DrawManager(sequence: sequences[sender.tag])
        drawManager?.playSequence()
    }
}
