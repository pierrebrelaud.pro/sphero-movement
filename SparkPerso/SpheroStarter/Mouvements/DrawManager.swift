//
//  DrawManager.swift
//  SparkPerso
//
//  Created by Pierre Brelaud on 24/11/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import Foundation

class DrawManager {
    
    var sequence:[Draw]
    var stopAll:Bool = false
    var heading:Double = 140
    var sphero:BoltToy? = nil
    
    init(sequence:[Draw]) {
        self.sequence = sequence
    }
    
    func stop(){
        self.sequence = []
        stopAll = true
        playMove(draw: Stop(heading: heading, sphero: sphero!)) {
            print("Secure stop is send")
        }
    }
    
    func playSequence() {
        self.startSequence(sequence: self.sequence)
    }
        
    func startSequence(sequence:[Draw]) {
        if sequence.count == 0 || stopAll {
            print("Sequence finished")
        } else {
            if let draw = sequence.first{
                playMove(draw: draw){
                    print("Move did finish")
                    print("Remove movement from sequence")
                    let seqMinusFirst = Array(sequence.dropFirst())
                    self.startSequence(sequence: seqMinusFirst)
                }
            }
        }
    }
    
    func clearSequence(){
        self.sequence = []
    }

    
    func playMove(draw: Draw, moveDidFinish: @escaping (() -> ())) {
        talkWithSDK(draw: draw)
        delay(draw.duration){
            moveDidFinish()
        }
    }
    
    func talkWithSDK(draw: Draw) {
        if(draw.move == .firstRotateRight) {
            draw.sphero.rotateAim(draw.heading)
        }
        else {
            draw.sphero.roll(heading: draw.heading, speed: Double(draw.speed))
        }
    }
    
    func sequenceDescription() -> String {
        
        var fullDescription = ""
        
        for draw in sequence {
            fullDescription += "\(draw.description) \n "
        }
        
        return fullDescription
    }
}
