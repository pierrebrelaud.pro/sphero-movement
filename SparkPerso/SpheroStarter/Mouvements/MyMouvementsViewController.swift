//
//  MyMouvementsViewController.swift
//  SparkPerso
//
//  Created by Pierre Brelaud on 28/10/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import UIKit
import SocketIO

class MyMouvementsViewController: UIViewController {
    
    var sequence = [BasicMove]()
    
    @IBOutlet weak var headingIncreaseTextView: UITextView!
    @IBOutlet weak var mouvementCountTextField: UITextView!
    
    var n:Int = 0
    var speed:Double = 100
    //var delay:UInt32 = 250000
    var rotateValue:Double = 25.0
    
    //update mouvement count
    var countIncrease:Int = 0 {
        didSet{ displayCurrentState() }
    }
    
    //curent heading updated every 'delay'
    var headingIncrease:Double = 1 {
        didSet{ displayCurrentHeadingState() }
    }
    
    
    var currentHeading:Double = 0
    var initHeading:Double = 35
    
    var increaseHeading:Double = 5
    
    func initHeadingBis(heading:Double) {
        //self.currentHeading = heading + increaseHeading
        SharedToyBox.instance.bolt?.roll(heading: self.currentHeading, speed: 0)
    }
    
    func selfRotate(startHeading:Double) {
        SharedToyBox.instance.bolt?.rotateAim(startHeading)
        playSelfRotate(n: 1, endHeading: startHeading + 90)
    }
    
    func playSelfRotate(n:Int, endHeading:Double) {

        if n > 21 || currentHeading > endHeading {
            //update init heading
            self.initHeading = endHeading
            //update current heading
            self.currentHeading = endHeading
            SharedToyBox.instance.bolt?.stopRoll(heading: self.currentHeading)
            return
        }
        
        if n == 1 {
            currentHeading = endHeading
            SharedToyBox.instance.bolt?.rotateAim(currentHeading)
        }
        else {
            SharedToyBox.instance.bolt?.roll(heading: self.currentHeading, speed: 40)
        }
        
        /*if n > 1 && n < 10 {
            SharedToyBox.instance.bolt?.rotateAim(self.currentHeading)
        }
        else {
            SharedToyBox.instance.bolt?.roll(heading: self.currentHeading, speed: 40)
        }*/

        
        self.currentHeading = self.currentHeading + increaseHeading
        
        //0.25
        delay(Float(0.1)) {
            self.playSelfRotate(n: n + 1, endHeading: endHeading)
        }
        print("heading : \(self.currentHeading)")
    }
    
    
    @IBAction func doCircleClicked(_ sender: Any) {
        //self.currentHeading = self.initHeading
        
        
        
        initHeadingFunc()
        delay(2) {
            self.currentHeading = self.initHeading + 135
            SharedToyBox.instance.bolt?.rotateAim(self.currentHeading)
            
            delay(2) {
                self.forward(v: 0.4)
                
                delay(1) {
                    self.currentHeading = self.initHeading + 90
                    SharedToyBox.instance.bolt?.rotateAim(self.currentHeading)
                    
                    delay(0.5) {
                        self.forward(v: 0.2)
                        
                        delay(0.5) {
                            self.currentHeading = self.currentHeading - 135
                            SharedToyBox.instance.bolt?.rotateAim(self.currentHeading)
                            delay(1) {
                                self.forward(v: 0.4)
                                
                                delay(1) {
                                    self.currentHeading = self.initHeading
                                    
                                    delay(0.5) {
                                        self.forward(v: 0.3)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
        }
        
        
        /*delay(2) {
            self.selfRotate(startHeading: self.initHeading)
        }*/
        
        /*delay(2) {
            self.forward(v:1)
            
            delay(3) {
                self.selfRotate()
                
                delay(3) {
                    self.forward(v:1)
                    
                    delay(3) {
                        self.selfRotate()
                        
                        delay(3) {
                            self.forward(v:1)
                            
                            delay(3) {
                                self.selfRotate()
                                
                                delay(3) {
                                    self.forward(v:1.3)
                                }
                            }
                        }
                    }
                }
            }
        }*/
        //---------------circle
        /*delay(3) {
            self.selfRotate()
        }
        delay(7) {
            self.selfRotate()
        }
        delay(11) {
            self.selfRotate()
        }
        delay(15) {
            self.selfRotate()
        }*/
        //---------------circle
        //---------------sequence
        /*delay(2) {
            self.forward()
        }
        delay(4) {
            self.doCircle()
        }
        delay(10) {
            self.forward()
        }*/
        //---------------sequence
        
    }
    
    func initHeadingFunc() {
        self.currentHeading = self.initHeading
        SharedToyBox.instance.bolt?.roll(heading: self.currentHeading, speed: 0)
    }
    
    func forward(v:Float) {
        SharedToyBox.instance.bolt?.roll(heading: self.currentHeading, speed: 110)
        delay(v) {
            SharedToyBox.instance.bolt?.stopRoll(heading: self.currentHeading)
        }
    }
    
    
    
    func displayCurrentHeadingState() {
        print("update : \(headingIncrease)")
        headingIncreaseTextView.text = "\(headingIncrease.rounded())"
    }
    
    func displayCurrentState() {
        mouvementCountTextField.text = "\(countIncrease)"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    
    @IBAction func countUpdate(_ sender: UISlider) {
        countIncrease = Int(sender.value)
    }
    
    @IBAction func headingUpdated(_ sender: UISlider) {
        headingIncrease = Double(sender.value).rounded()
    }
    
    
    func test2() {
        
        n -= 1
        
        SharedToyBox.instance.bolt?.roll(heading: self.currentHeading, speed: 50)
        //increment heading
        self.currentHeading = self.currentHeading + 15
        //last move
        
        delay(0.3) {
            SharedToyBox.instance.bolt?.stopRoll(heading: self.currentHeading)
        }
        
        if n > 0 {
            delay(1) {
                self.test2()
            }
        }
    }
    
    func test() {
        print("start test")
        self.currentHeading = self.initHeading + 90
        SharedToyBox.instance.bolt?.roll(heading: self.currentHeading, speed: 50)
        //usleep(1000)
        delay(0.5) {
            print("stop test")
            SharedToyBox.instance.bolt?.stopRoll(heading: self.currentHeading)
        }
    }
    
    
    func doCircle() {
        playCircleBis(n: 1)
    }
    
    
    func playCircleBis(n:Int) {
        print("heading : \(self.currentHeading)")
        if n > 21 {
            self.currentHeading = currentHeading - 94
            SharedToyBox.instance.bolt?.stopRoll(heading: self.currentHeading)
            return
        }
        
        if n == 1 {
            SharedToyBox.instance.bolt?.rotateAim(self.currentHeading + self.increaseHeading)
        }
        
        if n > 1 && n < 10 {
            SharedToyBox.instance.bolt?.rotateAim(self.currentHeading)
        }
        else {
            SharedToyBox.instance.bolt?.roll(heading: self.currentHeading, speed: 40)
        }

        
        self.currentHeading = self.currentHeading + increaseHeading
        
        //0.25
        delay(Float(0.1)) {
            self.playCircleBis(n: n + 1)
        }
        /*
        
        if n > 43 {
            SharedToyBox.instance.bolt?.stopRoll(heading: heading)
            return
        }

        SharedToyBox.instance.bolt?.roll(heading: self.heading, speed: 40)
        //increment heading
        self.heading = self.heading + 10.5
        
        //0.25
        delay(Float(0.1)) {
            self.playCircleBis(n: n+1)
        }
    */
        
    }
    
    
    @IBAction func stopClicked(_ sender: Any) {
        stop()
    }
    
    let matrix =
        [
            "shape": [
                "0": [1,2],
                "1":[2,2]
            ]
        ]
    
     // true
    
    @IBAction func smileyClicked(_ sender: UIButton) {
        print(matrix)
        //SharedToyBox.instance.bolt?.drawMatrix(pixel: Pixel(x: 1, y: 1), color: .red)
        
        if let data = matrix["shape"] {
            for shape in data {
                SharedToyBox.instance.bolt?.drawMatrix(pixel: Pixel(x: shape.value[0], y: shape.value[1]), color: .red)
            }
        }
        else {
            print("no data")
        }
    }
    
    
    @IBAction func serverConnectionClicked(_ sender: Any) {
        print("connect server clicked")
        SocketIoManager.instance.setup()
        SocketIoManager.instance.connect {
            SocketIoManager.instance.writeToChannel("plop", toChannel: "test") {
                
            }
            SocketIoManager.instance.listenToChannel(channel: "test") {
                data in
                if let d = data  {
                    //print()
                }
                //receivedStr in
                /*if let str = receivedStr {
                    print(str)
                }
                else {
                    print("wrong type")
                }*/
            }
        }
    }
    
    func playCircle(n:Int,count:Int) {
        
        print(n)
        if n > count {
            return
        }
        
        SharedToyBox.instance.bolt?.roll(heading: self.currentHeading, speed: self.speed)
        //increment heading
        self.currentHeading = self.currentHeading + rotateValue
        //last move
        if(n == count) {
            //stop sphero with current heading
            SharedToyBox.instance.bolt?.stopRoll(heading: currentHeading)
        }
        
        //0.25
        delay(Float(0.25)) {
            self.playCircle(n: n+1, count: count)
        }
        
    }
    
    func stop() {
        SharedToyBox.instance.bolt?.roll(heading: currentHeading, speed: 0.0)
    }
}

