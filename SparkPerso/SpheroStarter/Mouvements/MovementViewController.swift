//
//  MovementViewController.swift
//  SparkPerso
//
//  Created by Pierre Brelaud on 14/11/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import UIKit

class MovementViewController: UIViewController {
    
    @IBOutlet weak var speedValueLabel: UILabel!
    
    @IBOutlet weak var headingValueLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SharedToyBox.instance.bolt?.setBackLed(color: .green)
        // Do any additional setup after loading the view.
    }
    
    var test:Int = 0
    
    var speed:Double = 0 {
        didSet{ displayCurrentSpeedState() }
    }
    
    var currentHeading:Double = 1 {
        didSet{ displayCurrentHeadingState() }
    }
    
    var increaseHeading:Double = 5
    var increase:Double = 90/4.68
    var tempHeading:Double = 0
    
    //duration of 45° movement
    let rotateDuration:Float = 0.26
    
    @IBAction func roofButtonClicked(_ sender: Any) {
        forward(duration: 0.6)
        self.tempHeading = self.currentHeading + 45
        var endHeading = self.currentHeading + 180
        
        
        delay(2) {
            self.do90right()
            delay(1.5) {
                self.currentHeading += self.increase
                self.rotate(duration: 0.26)
                
                delay(2) {
                    self.updateHeading(heading: self.tempHeading)
                    delay(2) {
                        self.forward(duration: 0.5)
                        delay(2) {
                            self.moveRotate90Right()
                            
                            delay(8) {
                                self.forward(duration: 0.5)
                                
                                delay(3) {
                                    self.moveRotate45Right()
                                    
                                    delay(10) {
                                        self.updateHeading(heading: endHeading)
                                        
                                        delay(4) {
                                            self.forward(duration: 0.6)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func moveRotate45Right() {
        delay(2) {
            self.do90right()
            delay(1.5) {
                self.currentHeading += self.increase
                self.rotate(duration: 0.26)
            }
        }
    }
    
    func moveRotate90Right() {
        tempHeading = currentHeading + 90
        
        delay(2) {
            self.do90right()
            //45
            delay(1.5) {
                self.currentHeading += self.increase
                self.rotate(duration: 0.26)
                
                //90
                delay(2) {
                    self.currentHeading += (self.increase + 16)
                    self.rotate(duration: 0.26)
                    
                    delay(2) {
                        self.updateHeading(heading: self.tempHeading)
                    }
                }
            }
        }
    }
    
    func rotate90Move() {
        //rotate 90
        self.do90right()
        
        delay(1) {
            //first 45
            self.rotate45Right(first: true)
            //second 45
            delay(1) {
                self.rotate45Right(first: false)
            }
        }
    }
    
    func rotate45Right(first:Bool) {
        self.currentHeading += first ? self.increase : self.increase + 16
        self.rotate(duration: self.rotateDuration)
    }
    
    func rotate(duration:Float) {
        SharedToyBox.instance.bolt?.roll(heading: currentHeading, speed: speed)
        delay(duration) {
            self.stop()
            self.currentHeading += self.increase
            self.updateHeading(heading: self.currentHeading)
        }
    }
    
    func test2(last:Float) {
        tempHeading = currentHeading + 90
        
        delay(2) {
            self.do90right()
            delay(1.5) {
                self.currentHeading += self.increase
                self.rotate(duration: 0.26)
                
                delay(2) {
                    self.currentHeading += (self.increase + 16)
                    self.rotate(duration: 0.26)
                    
                    delay(2) {
                        self.updateHeading(heading: self.tempHeading)
                        delay(1) {
                            self.forward(duration: last)
                            
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func squareButtonClicked(_ sender: Any) {
        forward(duration: 0.8)
        
        delay(2) {
            self.test2(last:0.8)
            
            delay(12) {
                self.test2(last:0.8)
                
                delay(12) {
                    self.test2(last:0.8)
                }
            }
        }
    }
    
    func do90right() {
        currentHeading += 90
        updateHeading(heading: currentHeading)
    }
    
    func do90left() {
        currentHeading -= 90
        updateHeading(heading: currentHeading)
    }
    
    @IBAction func rotateButtonClicked(_ sender: Any) {
        
        if test > 0 {
            currentHeading += increase
        }
        else {
            currentHeading += (increase + 16)
        }
        test += 1
        
        
        delay(1) {
            self.rotate(duration: 0.35)
        }
    }
    
    @IBAction func plus90ButtonClicked(_ sender: Any) {
        forward(duration: 0.8)
        
        tempHeading = currentHeading + 90
        
        delay(1) {
            self.rotate90Move()
            delay(3) {
                self.updateHeading(heading: self.tempHeading)
                
                delay(0.5) {
                    self.forward(duration: 0.8)
                }
                
            }
        }
    }
    
    @IBAction func forwardButtonClicked(_ sender: Any) {
        forward(duration: 0.4)
    }
    
    func forward(duration:Float) {
        SharedToyBox.instance.bolt?.roll(heading: currentHeading, speed: speed)
        delay(duration) {
            self.stop()
        }
    }
    
    
    func stop() {
        SharedToyBox.instance.bolt?.stopRoll(heading: currentHeading)
    }
    
    func displayCurrentHeadingState() {
        headingValueLabel.text = "\(currentHeading.rounded())"
    }
    
    func displayCurrentSpeedState() {
        speedValueLabel.text = "\(speed.rounded())"
    }
    
    @IBAction func speedSliderUpdated(_ sender: UISlider) {
        speed = Double(sender.value).rounded()
    }
    
    @IBAction func headingSliderUpdated(_ sender: UISlider) {
        currentHeading = Double(sender.value).rounded()
        SharedToyBox.instance.bolt?.rotateAim(currentHeading)
    }
    
    func updateHeading(heading:Double) {
        currentHeading = heading
        SharedToyBox.instance.bolt?.roll(heading: currentHeading, speed: 0)
    }
    
    func selfRotate(startHeading:Double) {
        SharedToyBox.instance.bolt?.rotateAim(startHeading)
    }
    
    func playSelfRotate(n:Int, endHeading:Double) {

        if n > 21 || currentHeading > endHeading {
            //update init heading
            self.currentHeading = endHeading
            //update current heading
            self.currentHeading = endHeading
            SharedToyBox.instance.bolt?.stopRoll(heading: self.currentHeading)
            return
        }
        
        if n == 1 {
            currentHeading = endHeading
            SharedToyBox.instance.bolt?.rotateAim(currentHeading)
        }
        else {
            SharedToyBox.instance.bolt?.roll(heading: self.currentHeading, speed: 40)
        }
        
        self.currentHeading = self.currentHeading + increaseHeading
        
        delay(Float(0.1)) {
            self.playSelfRotate(n: n + 1, endHeading: endHeading)
        }
        print("heading : \(self.currentHeading)")
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
