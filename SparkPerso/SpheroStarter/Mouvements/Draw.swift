//
//  Draw.swift
//  SparkPerso
//
//  Created by Pierre Brelaud on 24/11/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import Foundation

import Foundation

class Draw {
    var duration:Float
    var speed:Float = 250
    var move:Move
    var heading:Double
    var sphero:BoltToy
    
    enum Move{
        case forward, stop, firstRotateRight, rotate90Right, rotate45Right
    }
    var description:String{
        get{
            return "Move \(move) during \(duration)s with \(speed) speed, heading : \(heading)"
        }
    }
    
    init(duration:Float, move:Move, speed:Float, heading:Double, sphero:BoltToy) {
        print("init heading : \(heading)")
        self.duration = duration
        self.move     = move
        self.speed    = speed
        self.heading  = heading
        self.sphero   = sphero
    }
}

class Stop:Draw {
    init(heading:Double, sphero:BoltToy) {
        super.init(duration: 2, move: .stop, speed: 0, heading: heading, sphero:sphero)
    }
}

class Forward:Draw {
    init(duration:Float, heading:Double, sphero:BoltToy) {
        super.init(duration: duration, move: .forward, speed: 250, heading: heading, sphero:sphero)
    }
}

class FirstRotate:Draw {
    init(duration:Float, heading:Double, sphero:BoltToy) {
        super.init(duration: duration, move: .firstRotateRight, speed: 250, heading: heading, sphero:sphero)
    }
}

class Heading:Draw {
    init(duration:Float, heading:Double, sphero:BoltToy) {
        super.init(duration: duration, move: .firstRotateRight, speed: 0, heading: heading, sphero:sphero)
    }
}

class Rotate45Right:Draw {
    init(duration:Float, heading:Double, sphero:BoltToy) {
        super.init(duration: duration, move: .rotate45Right, speed: 250, heading: heading, sphero:sphero)
    }
}

class Rotate90Right:Draw {
    init(duration:Float, heading:Double, sphero:BoltToy) {
        super.init(duration: duration, move: .rotate90Right, speed: 250, heading: heading, sphero:sphero)
    }
}
