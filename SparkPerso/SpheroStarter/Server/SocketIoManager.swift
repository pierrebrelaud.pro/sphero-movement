//
//  SocketIoManager.swift
//  SparkPerso
//
//  Created by Pierre Brelaud on 30/10/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import Foundation
import SocketIO

class SocketIoManager {
    
    struct Ctx {
        
        var ip:String
        var port:String
        var modeVerbose:Bool
        //...
        
        func fullIp() -> String {
            return "http://" + ip + ":" + port
        }
        //172.28.59.135
        static func debugContext() -> Ctx {
            return Ctx(ip: "192.168.16.18", port: "3000", modeVerbose: false)
        }
    }
    
    //"http://localhost:3000"
    static let instance = SocketIoManager()
    
    var manager:SocketManager? = nil
    var socket:SocketIOClient? = nil
        
    func setup(ctx:Ctx = Ctx.debugContext()) {
        manager = SocketManager(socketURL: URL(string: ctx.fullIp())!, config: [.log(ctx.modeVerbose), .compress])
        socket = manager?.defaultSocket
    }
    
    func connect(callback:@escaping () -> ()) {
        listenToConnection(callback: callback)
        socket?.connect()
    }
    
    func listenToConnection(callback:@escaping () -> ()) {
        socket?.on(clientEvent: .connect) {data, ack in
            print("socket connected")
            callback()
        }
    }
    
    func listenToChannel(channel:String, callback:@escaping (Any?) -> ()) {
        socket?.on(channel) {data, ack in
            //print(data)
            print(type(of: data))

            if let d = data.first {
                
                callback(d)
                if let dictionary = d as? [String: Any] {
                    for (key, value) in dictionary {
                        // access all key / value pairs in dictionary
                        print("---------------------------")
                        //print(key)
                        //print(value)
                        print("---------------------------")
                    }
                }
            }
            else {
                callback(nil)
            }
            ack.with("Got your currentAmount", "dude")
        }
    }
    
    func writeToChannel(_ value:String, toChannel channel:String, callback:@escaping () -> ()) {
        socket?.emit(channel, value)
    }
    
    //
    /*= manager.defaultSocket
    

    socket.on(clientEvent: .connect) {data, ack in
        print("socket connected")
    }

    socket.on("currentAmount") {data, ack in
        guard let cur = data[0] as? Double else { return }
        
        socket.emitWithAck("canUpdate", cur).timingOut(after: 0) {data in
            socket.emit("update", ["amount": cur + 2.50])
        }

        ack.with("Got your currentAmount", "dude")
    }

    socket.connect()
    */
    
    
}
