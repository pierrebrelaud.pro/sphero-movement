//
//  SpheroTP1ViewController.swift
//  SparkPerso
//
//  Created by Guillaume Lagouy on 18/10/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import UIKit

class SpheroTP1ViewController: UIViewController {
    
    var spheroMovementManager:SpheroMovementManager? = nil
    
    @IBOutlet weak var sequenceTextView: UITextView!
    
    var sequence = [BasicMove](){
        didSet{
            DispatchQueue.main.async{
                self.displaySequenceOnTextView()
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*SocketIoManager.instance.setup()
        SocketIoManager.instance.connect {
            SocketIoManager.instance.listenToChannel(channel: "channel") {
                receivedStr in
                if let str = receivedStr {
                    self.manageCommand(str: str)
                }
                else {
                    print("wrong type")
                }
            }
        }*/
        
    }
    
    func manageCommand(str: String) {
        switch str {
        case let t where str.contains("FRONT"):
            
            var components = str.split(separator: ":")
            
            var parameters = Array(components.dropFirst())
            
            if let h = Double(parameters[0]),
                let d = Float(parameters[1]),
                let s = Float(parameters[2]) {
                self.sequence.append(SpheroMove(heading: h, durationInSec: d, speed: s))
            }
            
        case "LEFT":
            self.sequence.append(SpheroMove(heading: 90.0, durationInSec: 1.0, speed: 0.0))
        case "RIGHT":
            self.sequence.append(SpheroMove(heading: 270.0, durationInSec: 1.0, speed: 0.0))
        case "STOP": spheroMovementManager?.redAlert()
        default: break
            
        }
    }
    
    @IBAction func stopButtonClicked(_ sender: Any) {
        print("Stop")
        spheroMovementManager?.redAlert()
    }
    
    @IBAction func startButtonClicked(_ sender: Any) {
        print("Start")
        
        spheroMovementManager?.playSequence()
    }
    
    
    @IBAction func testButtonClicked(_ sender: Any) {
        print("click")
        SharedToyBox.instance.bolt?.drawMatrixLine(from: Pixel(x: 0, y: 0), to: Pixel(x: 20, y: 20), color: UIColor.red)
    }
    
    
    @IBAction func rotateLeftClicked(_ sender: Any) {
        self.sequence.append(SpheroMove(heading: 90.0, durationInSec: 1.0, speed: 0.0))
        
    }
    
    @IBAction func frontClicked(_ sender: Any) {
        self.askForDurationAndSpeedAndHeading{(speed, duration, heading) in
            self.sequence.append(SpheroMove(heading: Double(heading), durationInSec: duration, speed: speed))
        }
    }
    
    @IBAction func rotateRightClicked(_ sender: Any) {
        self.sequence.append(SpheroMove(heading: 270.0, durationInSec: 1.0, speed: 0.0))
        
    }
    
    @IBAction func displaySequenceClicked(_ sender: Any) {
        displaySequenceOnTextView()
        
    }
    
    @IBAction func clearSequenceClicked(_ sender: Any) {
        sequence = []
        sequenceTextView.text = ""
        spheroMovementManager?.clearSequence()
    }
    
    func displaySequenceOnTextView(){
        spheroMovementManager = SpheroMovementManager(sequence: sequence)
        if let desc = spheroMovementManager?.sequenceDescription(){
            sequenceTextView.text = desc
        }
    }
    
}
